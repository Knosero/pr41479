#!/usr/bin/perl

#======================
# RAFAŁ PILECKI pr41479
# Zadanie z lab 2
# Na ocene 4
#======================

use strict;
use warnings;


my $ocena = 4; #USTAWIENIE NA JAKĄ OCENE

my $start = 0;
my $end = 0;
my $it = 0;

my @listaprzedmiot = ();
my @godzinyprzedmiot = ();

#przeszukanie wybranego kalendarza ./ics.pl <  plan_zajec.ics
#============================================================
foreach my $line ( <STDIN> ) {
    chomp( $line );

    #zapisanie lini z startem zajeć
    #==============================
    if ($line =~ m/^DTSTART/)
    {
        $start = $line;
    }
    #zapisanie lini z końcem zajeć
    #=============================
    if ($line =~ m/^DTEND/)
    {
        $end = $line;
    }
    #wyliczenie różnicy
    #==================
    if ($line =~ m/^SUMMARY/)
    {
        my $name = $line;
        
        #WYCIĄGNIĘCIE Z STRINGA NAZWY ZAJĘĆ
        #==================================
        my @namehelp = split(/:/,$name);
        my $orginalname = $namehelp[1];
        
        #WYCIĄGNIĘCIE STRINGA GODZINY 
        #============================
        my @tabhelp1 = split( /:/, $start);
        my @tabhelp2 = split( /:/, $end);
        $start = $tabhelp1[1];
        $end = $tabhelp2[1];
        my @tabStart = split( /T/, $start);
        my @tabEnd = split( /T/, $end);
        
        #WYLICZENIE GODZIN
        #=================
        $start = sprintf("%d", $tabStart[1]) / 10000 * 60;
        $end = sprintf("%d", $tabEnd[1]) / 10000 * 60;
        my $new = ($end - $start) / 60;

        if($ocena == 3){
            $it += $new;
        }
        
        if($ocena == 4){
            #PRZESZUKANIE PRZEDMIOTÓW
            #------------------------
            my $length = @listaprzedmiot;
            my $k = 0;
            for(my $i = 0; $i < $length; $i++){
                if($orginalname eq $listaprzedmiot[$i]){
                    $k = 1;
                    $godzinyprzedmiot[$i] += $new;
                }
            }

            #DODANIE NOWEGO PRZEMIOTU
            #------------------------
            if($k == 0){
                @listaprzedmiot = (@listaprzedmiot, $orginalname);
                @godzinyprzedmiot = (@godzinyprzedmiot, $new);
            }
        }
    }
}

#WYISYWANIE GODZIN
#=================
if($ocena == 3){
    print "$it\n"; #wynik na ocene 3 jakby coś nie pasowało w 4
}

if($ocena == 4){
    my $l = @listaprzedmiot;
    for (my $i = 0; $i < $l; $i++){
        print "$listaprzedmiot[$i] ma $godzinyprzedmiot[$i]\n";
    }
}