#!/usr/bin/perl

#======================
# RAFAŁ PILECKI pr41479
# Zadanie z lab 1
# Na ocene 4
#======================

#FLAGI
#===== 
my $l1 = 0;
my $l2 = 0;

#POBIERANIE LOKALIZACJ PROGRAMU
#==============================
use Cwd;
my $dir = getcwd;

#WALIDACJA FLAG
#==============
my $numArgs = $#ARGV + 1;
for (my $i = 0; $i < $numArgs; $i++) {
    if($ARGV[$i] eq "-l")
    {
        $l1 = 1;
    }
    elsif($ARGV[$i] eq "-L")
    {
        $l2 = 1;
    }
    else
    {
        $dir = $ARGV[$i];
    }
}

#OTWORZENIE KATALOGU
#===================
opendir(DIR, $dir) or die $!;
while (my $file = readdir(DIR)) 
{
    next if $file =~/^\./;
    
    my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat($file);
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($mtime);
    
    $year += 1900;
    
    if($l1 == 1)
    {
        my $p = $mode & 07777;
        my $per = sprintf("%o", $p);
        if($mode < 20000)   {print "d";} else {print "-";}
        
        #MODE rwd
        #========
        my $k = 0;
        if($per >= 700)    {print "rwx"; $k += 700;}
        elsif($per >= 600) {print "rw-"; $k += 600;}
        elsif($per >= 500) {print "r-x"; $k += 500;}
        elsif($per >= 400) {print "r--"; $k += 400;}
        elsif($per >= 300) {print "-wx"; $k += 300;}
        elsif($per >= 200) {print "-w-"; $k += 200;}
        elsif($per >= 100) {print "--x"; $k += 100;}
        else               {print "---";}

        if ($per >= ($k+70))   {print "rwx"; $k += 70;}
        elsif($per >= ($k+60)) {print "rw-"; $k += 60;}
        elsif($per >= ($k+50)) {print "r-x"; $k += 50;}
        elsif($per >= ($k+40)) {print "r--"; $k += 40;}
        elsif($per >= ($k+30)) {print "-wx"; $k += 30;}
        elsif($per >= ($k+20)) {print "-w-"; $k += 20;}
        elsif($per >= ($k+10)) {print "--x"; $k += 10;}
        else {print "---";}

        if ($per >= ($k+7))   {print "rwx";}
        elsif($per >= ($k+6)) {print "rw-";}
        elsif($per >= ($k+5)) {print "r-x";}
        elsif($per >= ($k+4)) {print "r--";}
        elsif($per >= ($k+3)) {print "-wx";}
        elsif($per >= ($k+2)) {print "-w-";}
        elsif($per >= ($k+1)) {print "--x";}
        else {print "---";}

        #Date
        #====
        print "\t$size $year-$mon-$mday $hour:$min:$sec\t";
    }

    #Name File
    #=========
    print ("$file\n");

}

closedir(DIR);